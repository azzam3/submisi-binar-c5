const { json } = require('express');
const express = require('express');
const app = express();

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
};
app.use(express.urlencoded({ extended: false }));

app.use(logger);

const router = require('./router');
app.use(router);

app.set('view engine', 'ejs');
app.use(express.static(__dirname));
app.use(express.json());

const products = ["Lenovo", "Asus", "Dell"];
const orders = [
    {
        id: 1,
        paid: false,
        user_id: 2
    },
    {
        id: 2,
        paid: true,
        user_id: 1
    }
];

app.get('/error', (req, res) => {
    error;
});

app.get('/products', (req, res) => {
    let result = {
        success: true,
        message: 'Success',
        code: 200,
        products: products
    };
    return res.status(result.code).json(result);
})

app.get('/orders', (req, res) => {
    let result = {
        success: true,
        message: 'Success',
        code: 200,
        orders: orders
    };
    return res.status(result.code).json(result);
});

app.get('/order/:id', (req, res) => {
    const order = orders.find(o => o.id == req.params.id);
    let result = {
        success: false,
        message: 'Not Found',
        code: 404
    };
    if (!order) return res.status(result.code).json(result);
    else {
        result = {
            success: true,
            message: 'Success',
            code: 200,
            orders: order
        };
        return res.status(result.code).json(result);
    }
});

app.post('/products', (req, res) => {
    const result = productValidation(req.body);

    if (!result.success) return res.status(result.code).json(result);
    else {
        products.push(req.body.name);
        result.products = products;
        return res.status(result.code).json(result);
    }
});

app.post('/orders', (req, res) => {
    const result = orderValidation(req.body);
    const order = result.payload;
    if (!result.success) return res.status(result.code).json(result);
    else {
        orders.push(order);
        result.orders = orders;
        return res.status(result.code).json(result);
    }
});

app.put('/orders/:id', (req, res) => {
    const order = orders.find(o => o.id == req.params.id);
    const notFound = {
        success: false,
        message: 'Not Found',
        code: 404
    };
    if (!order) return res.status(notFound.code).json(notFound);

    const result = orderUpdate(req.body, order);
    const payload = result.payload;
    if (!result.success) return res.status(result.code).json(result);
    else {
        result.orders = orders;
        return res.status(result.code).json(result);
    }
});

app.delete('/order/:id', (req, res) => {
    const order = orders.find(c => c.id == req.params.id);
    const notFound = {
        success: false,
        message: 'Not Found',
        code: 404
    };
    if (!order) return res.status(notFound.code).json(notFound);
    else {
        const index = orders.indexOf(order);
        orders.splice(index, 1);

        let result = {
            success: true,
            message: 'Success',
            code: 200
        };
        return res.status(result.code).json(result);
    }
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        message: 'Path not found'
    });
});

// Port
const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Listening at http://localhost:${port}`));

function productValidation(variable) {
    let result = {
        success: false,
        message: 'Failed',
        code: 400
    }
    if (typeof variable.name !== 'undefined') {
        result = {
            success: true,
            message: 'Success',
            code: 200
        }
    }
    return result;
}

function orderValidation(variable) {
    let result = {
        success: false,
        message: 'Failed',
        code: 400,
        payload: false
    }
    variable.user_id = Number(variable.user_id);
    variable.paid = parseBool(variable.paid);
    console.log(typeof variable.paid);
    if (typeof variable.paid !== 'undefined' && typeof variable.user_id !== 'undefined') {
        result = {
            success: true,
            message: 'Success',
            code: 200,
            payload: { id: orders.length + 1, paid: variable.paid, user_id: variable.user_id }
        }
    }
    return result;
}

function orderUpdate(variable, order) {
    let result = {
        success: false,
        message: 'Failed',
        code: 400,
        payload: false
    }
    variable.user_id = Number(variable.user_id);
    variable.paid = parseBool(variable.paid);
    console.log(typeof variable.paid);
    if (typeof variable.paid !== 'undefined' && typeof variable.user_id !== 'undefined') {
        order.paid = variable.paid;
        order.user_id = variable.user_id;

        result = {
            success: true,
            message: 'Success',
            code: 200,
            payload: { id: order.id, paid: variable.paid, user_id: variable.user_id }
        }
    }
    return result;
}

function parseBool(val) {
    if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1)
        return true;
    else if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0)
        return false;
    return undefined;
}
