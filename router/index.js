const express = require('express');
const router = express.Router();

let users = [
    {
        id: 1,
        username: 'Yuksuit',
        email: 'yuksuit@email.com',
        password: '123456'
    }
];

let loggedIn = {
    id: null,
    username: null,
    email: null,
    password: null
}

router.use(function timeLog(res, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.get('/', (req, res, next) => {
    res.render('homepage', { loggedIn });
});

router.get('/game', (req, res) => {
    res.render('game');
});

router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/register', (req, res) => {
    res.render('register');
});

router.post('/login', (req, res) => {
    const { email, password } = req.body;

    const order = users.find(found => found.email == email);
    if (order && order.password == password) {
        loggedIn = order;
        res.redirect('/');
    }
    res.redirect('/login')
});

router.post('/register', (req, res) => {
    const { username, email, password } = req.body;

    const registered = {
        id: users.length + 1,
        username: username,
        email: email,
        password: password
    }

    users.push(registered);

    res.redirect('/login')
});

router.get('/logout', (req, res) => {
    loggedIn = {
        id: null,
        username: null,
        email: null,
        password: null
    }
    res.redirect('/')
});

module.exports = router;
